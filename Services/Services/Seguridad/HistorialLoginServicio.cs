﻿using Domain.Entidades.Seguridad;
using Domain.Entidades.Util;
using Microsoft.Extensions.Logging;
using Repository;
using Repository.Repositorios.Seguridad;
using Services.IServices;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services.Seguridad
{
    public class HistorialLoginServicio : IHistorialLoginServicio
    {
        private readonly HistorialLoginRepositorio _historialLoginRepositorio;
        private readonly ILogger _logger;

        public HistorialLoginServicio(ApplicationDbContext context, ILogger<HistorialLoginRepositorio> logger)
        {
            _historialLoginRepositorio = new HistorialLoginRepositorio(context);
            _logger = logger;
        }

        public async Task<ResponseHelper> Crear(HistorialLogin historialLogin)
        {
            var response = new ResponseHelper();
            try
            {
                if (await _historialLoginRepositorio.Crear(historialLogin) > 0)
                {
                    response.Success = true;
                    response.Message = "login registrado en historial";
                    _logger.LogInformation(response.Message);
                }

            }
            catch (Exception e)
            {
                response.Success = false;
                response.Message = "Error al registrar login";
                _logger.LogInformation(e.Message);
            }
            return response;
        }
    }
}
