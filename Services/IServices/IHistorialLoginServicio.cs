﻿using Domain.Entidades.Seguridad;
using Domain.Entidades.Util;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.IServices
{
    public interface IHistorialLoginServicio
    {
        Task<ResponseHelper> Crear(HistorialLogin historialLogin);
    }
}
