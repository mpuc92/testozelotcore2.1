﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BaseWeb.Extenciones;
using Domain.Entidades.Seguridad;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Services.IServices;

namespace BaseWeb.Controllers.Seguridad
{
    [Layout("_AuthLayout")]
    public class AccountController : Controller
    {
        private readonly ILogger _logger;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        //private readonly RoleManager<AspNetRole> _roleManager;
        private readonly IHistorialLoginServicio _historialLogin;

        private HistorialLogin historialLogin;
        private static string proyecto = "Diabetes";


        [TempData]
        public string ErrorMessage { get; set; }

        public AccountController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, ILogger<AccountController> logger, IHistorialLoginServicio historialLoginServicio)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _historialLogin = historialLoginServicio;
            //_roleManager = roleManager;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            historialLogin = new HistorialLogin
            {
                Username = !string.IsNullOrEmpty(model.Email) ? model.Email : "direccin email vacia",
                Proyecto = proyecto,
                DireccionIP = Request.Host.Value
            };

            if (ModelState.IsValid)
            {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var us = await _userManager.FindByEmailAsync(model.Email);
                if (us != null)
                {
                    if (us.IsDeleted == false)
                    {
                        if (await _userManager.IsEmailConfirmedAsync(us))
                        {

                            var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                            if (result.Succeeded)
                            {
                                historialLogin.Acceso = "Exitoso";
                                await _historialLogin.Crear(historialLogin);


                                _logger.LogInformation("User logged in.");
                                return RedirectToLocal(returnUrl);
                            }
                            else
                            {
                                historialLogin.Acceso = "Fallido";
                                await _historialLogin.Crear(historialLogin);


                                TempData["Error"] = "al iniciar sesión. Verificar el correo y contraseña.";
                            }
                            if (result.IsLockedOut)
                            {
                                historialLogin.Acceso = "Bloqueado";
                                await _historialLogin.Crear(historialLogin);


                                _logger.LogWarning("User account locked out.");
                                return RedirectToAction(nameof(Lockout));
                            }
                            historialLogin.Acceso = "Fallido/Login-Invalido";
                            await _historialLogin.Crear(historialLogin);


                            TempData["Error"] = "al iniciar sesión. Verificar el correo y contraseña.";
                            return View(model);

                        }
                        else
                        {
                            historialLogin.Acceso = "Fallido/Email sin confirmar.";
                            await _historialLogin.Crear(historialLogin);
                            TempData["Error"] = "Esta cuenta necesita la confirmación de la Dirección Email.";

                            return View(model);
                        }
                    }
                    else
                    {
                        historialLogin.Acceso = "Fallido/Email no registrado";
                        await _historialLogin.Crear(historialLogin);
                        TempData["Error"] = "dirección email no registrada en el sistema.";
                        return View(model);
                    }
                }
                else
                {
                    historialLogin.Acceso = "Fallido/Email no registrado";
                    await _historialLogin.Crear(historialLogin);
                    TempData["Error"] = "dirección email no registrada en el sistema.";

                    return View(model);
                }


            }

            // If we got this far, something failed, redisplay form
            historialLogin.Acceso = "Fallido/Login-Invalido";
            await _historialLogin.Crear(historialLogin);
            TempData["Error"] = "al iniciar sesión. Verificar el correo y contraseña.";

            return View(model);
        }


        [HttpGet]
        [AllowAnonymous]
        public IActionResult Lockout()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;

            if (!ModelState.IsValid)
                return View(model);

            var user = new ApplicationUser
            {
                UserName = model.Email,
                Email = model.Email
            };

            var result = await _userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                _logger.LogInformation("User created a new account with password.");

                var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                var callbackUrl = Url.EmailConfirmationLink(user.Id, code, Request.Scheme);
                //await _emailSender.SendEmailConfirmationAsync(model.Email, callbackUrl);

                await _signInManager.SignInAsync(user, isPersistent: false);
                _logger.LogInformation("User created a new account with password.");
                return RedirectToLocal(returnUrl);
            }

            AddErrors(result);

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            _logger.LogInformation("User logged out.");
            return RedirectToAction(nameof(HomeController.Index), "Home");


        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{userId}'.");
            }
            var result = await _userManager.ConfirmEmailAsync(user, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            // Something failed, redisplay form
            if (!ModelState.IsValid)
                return View(model);

            // Check if the user exists in the data store
            var user = await _userManager.FindByEmailAsync(model.Email);

            // If no user is found, or the user has not confirmed their email address yet
            if (user == null) return View("ForgotPasswordConfirmationFail");

            //if (await _configuracionGenericaServicio.Existe("CONFING_CORREO")) // Si existe una configuracion de correo
            //{
            //    ConfiguracionGenerica configuracion_ = await _configuracionGenericaServicio.BuscarPorClave("CONFING_CORREO");
            //    ConfiguracionCorreo configuracionCorreo = JsonConvert.DeserializeObject<ConfiguracionCorreo>(configuracion_.Valor);
            //    if (configuracionCorreo.Habilitado) // la consiguracion esta habilitada?
            //    {
            //        PlantillaCorreo plantilla = new PlantillaCorreo();
            //        EnviarCorreo enviar = new EnviarCorreo();
            //        var domain = Request.Host.Value;
            //        var code = await _userManager.GeneratePasswordResetTokenAsync(user);
            //        var callbackUrl = Url.ResetPasswordCallbackLink(user.Id, code, Request.Scheme);

            //        string mensaje = plantilla.GetContrasenaRestauracion(user.UserName, callbackUrl, user.Email, domain);
            //        if (enviar.SendAsync(user.Email, "Restauración de contraseña", mensaje, configuracionCorreo.Correo_Destinatario, configuracionCorreo.Nombre))
            //        {
            //            return RedirectToAction(nameof(ForgotPasswordConfirmation));
            //        }
            //        else
            //        {//fallo
            //            return View("ForgotPasswordConfirmationFail");
            //        }
            //    }
            //    else
            //    {//la configuracion esta desactivada
            //        return View("ForgotPasswordConfirmationFail");
            //    }
            //}
            //else
            //{//no se ha creado la configuracion de correos
            //    return View("ForgotPasswordConfirmationFail");
            //}

            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string code = null)
        {
            if (code == null)
            {
                throw new ApplicationException("A code must be supplied for password reset.");
            }
            var model = new ResetPasswordViewModel
            {
                Code = code
            };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            AddErrors(result);
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }
    }
}