﻿using Domain.Entidades.Personas;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Entidades.OperacionAcademica
{
    [Table("AlumnosGrupos")]
    public class AlumnoGrupo
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Alumno")]
        public int? IdAlumno { get; set; }
        public virtual Alumno Alumno { get; set; }
        [ForeignKey("Grupo")]
        public int? IdGrupo { get; set; }
        public virtual Grupo Grupo { get; set; }
        public DateTime RowVersion { get; set; }
        public bool IsDeleted { get; set; }
        public string AppUser { get; set; }
    }
}
