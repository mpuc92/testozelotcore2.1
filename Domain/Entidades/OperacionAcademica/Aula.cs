﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Entidades.OperacionAcademica
{
    [Table("Aulas")]
    public class Aula
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Nombre { get; set; }
        [Range(1,50)]
        public int Capacidad { get; set; }
        public virtual ICollection<Grupo> Grupos { get; set; }
        public DateTime RowVersion { get; set; }
        public bool IsDeleted { get; set; }
        public string AppUser { get; set; }
    }
}
