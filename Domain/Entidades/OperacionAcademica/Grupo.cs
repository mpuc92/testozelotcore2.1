﻿using Domain.Entidades.Personas;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Entidades.OperacionAcademica
{
    [Table("Grupos")]
    public class Grupo
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Nombre { get; set; }
        [ForeignKey("Profesor")]
        public int? IdProfesor { get; set; }
        public virtual Profesor Profesor { get; set; }
        [ForeignKey("Aula")]
        public int? IdAula { get; set; }
        public virtual Aula Aula { get; set; }
        public virtual ICollection<AlumnoGrupo> AlumnosGrupos { get; set; }
        public DateTime RowVersion { get; set; }
        public bool IsDeleted { get; set; }
        public string AppUser { get; set; }
    }
}
