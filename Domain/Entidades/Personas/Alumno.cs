﻿using Domain.Entidades.OperacionAcademica;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain.Entidades.Personas
{
    public class Alumno : Persona
    {
        [StringLength(10)]
        [Required]
        public string Matricula { get; set; }
        public virtual ICollection<AlumnoGrupo> AlumnosGrupos { get; set; }
    }
}
