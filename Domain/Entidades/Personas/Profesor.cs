﻿using Domain.Entidades.OperacionAcademica;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain.Entidades.Personas
{
    public class Profesor : Persona
    {
        [Required]
        [StringLength(10)]
        public string NumEmpleado { get; set; }
        public virtual ICollection<Grupo> Grupos { get; set; }
    }
}
