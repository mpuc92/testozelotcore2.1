﻿using Domain.Entidades.Seguridad;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Entidades.Personas
{
    [Table("Personas")]
    public class Persona
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Debes proporcionar el campo Nombre")]
        [StringLength(50,ErrorMessage ="El campo nombre debe contener máximo 50 caracteres")]
        [Display(Name ="Nombre de la persona")]
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string CURP { get; set; }
        [DataType(DataType.Date)]
        public DateTime FechaNacimiento { get; set; }
        [ForeignKey("ApplicationUser")]
        public string IdApplicationUser { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        public DateTime RowVersion { get; set; }
        public bool IsDeleted { get; set; }
        public string AppUser { get; set; }
    }
}
