﻿using Domain.Entidades.Personas;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entidades.Seguridad
{
    public class ApplicationUser : IdentityUser
    {
        public bool IsDeleted { get; set; }
        public virtual Persona Persona { get; set; }
    }
}
